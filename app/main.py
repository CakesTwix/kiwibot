from aiogram import executor

import db
from config import dp
from db import async_init
from handlers.callback_query import allow_disable_18, get_tags, set_source, no_compression
from handlers.message.help import help_command
from handlers.message.last import send_last
from handlers.message.random import send_random
from handlers.message.settings import settings_command
from handlers.message.start import start_command
from handlers.message.tag import tag

if __name__ == "__main__":
    executor.start_polling(dp, on_startup=async_init)
