import aiohttp
from models.sourceModel import MoebooruModel


class Moebooru:
    domain = 'yande.re'
    get_url = f'https://{domain}/post.json?tags=rating:s'
    post_url = '/post.json?'
    rating = ["rating:s","rating:e"]
    tag = "tags"

    async def getLast(self,  nsfw=False, tags = ""):
        async with aiohttp.ClientSession() as session:
            if nsfw:
                self.get_url = self.get_url.replace(self.rating[0],self.rating[1] + ' ' + tags) 
            async with session.get(self.get_url) as get:
                result = await get.json()
                return MoebooruModel(**result[0])

    async def getRandom(self, nsfw=False, tags = ""):
        async with aiohttp.ClientSession() as session:
            if nsfw:
                self.get_url = self.get_url.replace(self.rating[0],self.rating[1])
            async with session.get(self.get_url + ' order:random ' + tags) as get:
                result = await get.json()
                return MoebooruModel(**result[0])

    
    async def getTagsByid(self, id):
        async with aiohttp.ClientSession() as session:
            async with session.get(self.get_url.replace(self.rating[0],"") + "id:" + str(id)) as get:
                tags = await get.json()
                tag = ''
                for tag_item in tags[0][self.tag].split():
                    tag += '#' + tag_item + ' '
                return tag[:4096]
