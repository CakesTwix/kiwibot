from sources.moebooru import Moebooru


class Hypnohub(Moebooru):
    domain = 'hypnohub.net'
    get_url = f'https://{domain}/post/index.json?tags=rating:s'
    post_url = '/post/index.json?'