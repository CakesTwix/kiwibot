from sources.moebooru import Moebooru


class Lolibooru(Moebooru):
    domain = 'lolibooru.moe'
    get_url = f'https://{domain}/post.json?tags=rating:s'