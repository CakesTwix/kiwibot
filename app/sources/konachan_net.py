from sources.moebooru import Moebooru


class Konachan_Net(Moebooru):
    domain = 'konachan.net'
    get_url = f'https://{domain}/post.json?tags=rating:s'