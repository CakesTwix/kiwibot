import importlib
import os

from aiogram import Bot, Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from pydantic import BaseSettings

# Динамическое импортирование источников
folder = '/'.join(os.path.abspath(__file__).split('/')[:-1])  # .../kiwibot/app
modules_name = [
    name[:-3] for name in os.listdir(f"{folder}/sources") if name.endswith('.py')  # booru moebooru
]
modules = [importlib.import_module(f"sources.{name}") for name in modules_name]
sources_class = [
    getattr(module, name.title()) for name, module in zip(modules_name, modules)
]
sources = {source.domain: source for source in sources_class}
users = {}


class Settings(BaseSettings):
    TOKEN: str
    DEFAULT_SOURCE: str
    DB_ECHO: bool
    
    class Config:
        env_file = '.env'
        env_file_encoding = 'utf-8'
        case_sensitive = True


settings = Settings()

storage = MemoryStorage()
bot = Bot(settings.TOKEN)
dp = Dispatcher(bot, storage=storage)
