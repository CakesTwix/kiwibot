from aiogram.types import Message, InlineKeyboardButton, InlineKeyboardMarkup
from sqlalchemy import select

from db import MainTable, engine

from config import dp, sources

Source_Keyboard = InlineKeyboardMarkup()
for source in sources:
    Source_Keyboard.add(InlineKeyboardButton(source, callback_data=source))


@dp.message_handler(commands=["settings"])
@dp.throttled(rate=1)
async def settings_command(message: Message):
    async with engine.begin() as conn:
        result = await conn.execute(select(MainTable).where(MainTable.user_id == message.from_user.id))
        result_first = result.first()

        await message.answer(f"\n\nВыбран источник: {result_first[1]}"
                             f"\n\nДля изменения источника, нажмите на кнопки", reply_markup=Source_Keyboard)
