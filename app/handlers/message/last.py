from aiogram.types import (InlineKeyboardButton, InlineKeyboardMarkup,
                           InputFile, Message)
from aiogram.utils.callback_data import CallbackData
from config import bot, dp, sources
from db import MainTable, engine
from loguru import logger
from sqlalchemy import select

ArtCallbackData = CallbackData('Act', 'action', 'id', 'source')


@dp.message_handler(commands=["last", "nlast"])
@dp.throttled(rate=1)
async def send_last(message: Message):
    async with engine.begin() as conn:
        result = await conn.execute(select(MainTable).where(MainTable.user_id == message.from_user.id))
        result_first = result.first()
        class_ = sources[result_first[1]]()
        if message.get_command() == "/nlast":
            class_ = await class_.getLast(nsfw=True)
        else:
            class_ = await class_.getLast()
        if class_.file_size < 10000000:  # 10mb
            ArtMenuButton = InlineKeyboardMarkup(row_width=3)
            ArtMenuButton.add(InlineKeyboardButton('Получить теги', callback_data=ArtCallbackData.new(action='get_tags',
                                                                                             id=class_.id,
                                                                                             source=result_first[1])))
            ArtMenuButton.add(InlineKeyboardButton('Без сжатия', callback_data=ArtCallbackData.new(action='no_compression',
                                                                                             id=class_.id,
                                                                                             source=result_first[1])))                                                                
            await bot.send_chat_action(message.chat.id, 'upload_photo')
            await bot.send_photo(
                chat_id=message.chat.id,
                photo=InputFile.from_url(class_.sample_url),
                reply_to_message_id=message.message_id,
                reply_markup=ArtMenuButton)
            logger.info(f"Последний арт | {message.from_user.full_name}")
        else:
            logger.warning(f"Последний арт | Большой арт | {message.from_user.full_name}")
            await message.answer("Арт большой.... не смогу отправить вам")
