from aiogram.types import InlineKeyboardButton, InputFile, Message, InlineKeyboardMarkup
from config import bot, dp, sources
from db import MainTable, engine
from handlers.message.last import ArtCallbackData
from loguru import logger
from sqlalchemy import select


@dp.message_handler(commands=["random", "nrandom"])
@dp.throttled(rate=1)
async def send_random(message: Message):
    async with engine.begin() as conn:
        result = await conn.execute(select(MainTable).where(MainTable.user_id == message.from_user.id))
        result_first = result.first()
        class_ = sources[result_first[1]]()

        # Получаем данные арта
        if message.get_command() == "/nrandom":
            class_ = await class_.getRandom(nsfw=True)
        else:
            class_ = await class_.getRandom()
        
        # Проверяем вес арта, ограничение у тг в 10МБ
        if class_.file_size < 10000000:  # 10mb
            ArtMenuButton = InlineKeyboardMarkup(row_width=3)
            ArtMenuButton.add(InlineKeyboardButton('Получить теги', callback_data=ArtCallbackData.new(action='get_tags',
                                                                                             id=class_.id,
                                                                                             source=result_first[1])))
            ArtMenuButton.add(InlineKeyboardButton('Без сжатия', callback_data=ArtCallbackData.new(action='no_compression',
                                                                                             id=class_.id,
                                                                                             source=result_first[1])))                                                                                 
            await bot.send_chat_action(message.chat.id, 'upload_photo')
            await bot.send_photo(
                chat_id=message.chat.id,
                photo=InputFile.from_url(class_.sample_url),
                reply_to_message_id=message.message_id,
                reply_markup=ArtMenuButton)
            logger.info(f"Рандомный арт | {message.from_user.full_name}")
        else:
            logger.warning(f"Рандомный арт | Большой арт | {message.from_user.full_name}")
            await message.answer("Арт большой.... не смогу отправить вам")
