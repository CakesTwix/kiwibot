from aiogram.types.message import Message
from config import dp


@dp.message_handler(commands=["help"])
@dp.throttled(rate=1)
async def help_command(message: Message):
    await message.answer(f"Чтобы получить 18+ арты, следует к last/random добавить букву n, т.е\n"
                         f"└─ /nlast, /nrandom\n"
                         f"Для смены источника артов, нажмите на\n"
                         f"└─ /settings\n"
                         f"Чтобы использовать поиск, введите /tag <теги>\n"
                         f"├─ /tag loli school*\n"
                         f"└─ /ntag loli school*")
