from aiogram.types import Message
from sqlalchemy import insert, select
from sqlalchemy.exc import IntegrityError

from db import MainTable, engine

from config import dp


@dp.message_handler(commands=["start"])
@dp.throttled(rate=1)
async def start_command(message: Message):
    try:
        async with engine.begin() as conn:
            await conn.execute(insert(MainTable).values(user_id=message.from_user.id))
    except IntegrityError:
        pass
        #async with engine.begin() as conn:
        #    result = await conn.execute(select(MainTable).where(MainTable.user_id == message.from_user.id))
        #    await message.answer(result.first()) # (360862309, 'yande.re')
