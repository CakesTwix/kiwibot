from aiogram.types import CallbackQuery
from sqlalchemy import update

from config import dp, sources, bot
from db import engine, MainTable
from handlers.message.settings import Source_Keyboard


@dp.callback_query_handler(lambda c: c.data and c.data in sources)
async def set_source(callback_query: CallbackQuery):
    async with engine.begin() as conn:
        await conn.execute(update(MainTable).where(MainTable.user_id == callback_query.from_user.id).values(
            user_source=callback_query.data))
        await callback_query.answer(f"Вы выбрали - {callback_query.data}")
        await bot.edit_message_text(message_id=callback_query.message.message_id, chat_id=callback_query.message.chat.id,
                                    text=f"Выбран источник: {callback_query.data}"
                                    f"\nДля изменения источника, нажмите на кнопки", reply_markup=Source_Keyboard)
