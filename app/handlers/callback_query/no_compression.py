from aiogram.types import InputFile
from aiogram.types.callback_query import CallbackQuery
from aiogram.utils.callback_data import CallbackData
import aiohttp
from config import bot, dp, sources
from handlers.message.last import ArtCallbackData


@dp.callback_query_handler(ArtCallbackData.filter(action=['no_compression']))
async def no_compression(callback_query: CallbackQuery, callback_data: CallbackData):
    class_ = sources[callback_data["source"]]()
    async with aiohttp.ClientSession() as session:
            async with session.get("https://" + callback_data["source"] + class_.post_url + "tags=id:" + str(callback_data["id"])) as get:
                result = await get.json()

    await bot.send_chat_action(callback_query.message.chat.id, 'upload_document')
    await bot.send_document(callback_query.message.chat.id, InputFile.from_url(result[0]["file_url"]), reply_to_message_id=callback_query.message.message_id)