from aiogram.types import CallbackQuery
from aiogram.utils.callback_data import CallbackData
from config import bot, dp, sources
from handlers.message.last import ArtCallbackData


@dp.callback_query_handler(ArtCallbackData.filter(action=['get_tags']))
async def allow_18(callback_query: CallbackQuery, callback_data: CallbackData):
    class_ = sources[callback_data["source"]]()
    await bot.send_message(callback_query.message.chat.id, await class_.getTagsByid(callback_data["id"]), reply_to_message_id=callback_query.message.message_id)
