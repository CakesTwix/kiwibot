from aiogram.types import CallbackQuery
from aiogram.utils.exceptions import MessageNotModified

from config import dp, bot
from handlers.message.settings import Source_Keyboard


@dp.callback_query_handler(text="Allow_18+")
async def allow_18(callback_query: CallbackQuery):
    await callback_query.answer("Ты разрешил себе смотреть хентуй")
    try:
        await bot.edit_message_text(message_id=callback_query.message.message_id,
                                    chat_id=callback_query.message.chat.id,
                                    text=callback_query.message.text.replace("False", "True"),
                                    reply_markup=Source_Keyboard)
    except MessageNotModified:
        pass


@dp.callback_query_handler(text="Disable_18+")
async def allow_18(callback_query: CallbackQuery):
    await callback_query.answer("Ты запретил себе смотреть хентуй")
    try:
        await bot.edit_message_text(message_id=callback_query.message.message_id,
                                    chat_id=callback_query.message.chat.id,
                                    text=callback_query.message.text.replace("True", "False"),
                                    reply_markup=Source_Keyboard)
    except MessageNotModified:
        pass
