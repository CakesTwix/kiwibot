from aiogram import Dispatcher
from aiogram.types.bot_command import BotCommand
from sqlalchemy import Column, Integer, String, Boolean
from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.orm import declarative_base
from config import bot

from config import settings

base = declarative_base()


class MainTable(base):
    __tablename__ = "main_table"

    user_id = Column(Integer, primary_key=True)
    user_source = Column(String,  default=settings.DEFAULT_SOURCE)


engine = create_async_engine(
    "sqlite+aiosqlite:///db.sqlite3", echo=settings.DB_ECHO)


async def async_init(dispatcher: Dispatcher):
    await bot.set_my_commands([
        BotCommand("last", "Последний"),
        BotCommand("random", "Случайный"),
        BotCommand("help", "Помощь"),
        BotCommand("tag", "Поиск"),
    ])
    async with engine.begin() as conn:
        await conn.run_sync(base.metadata.create_all)
